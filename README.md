# 404k

カレントディレクトリ以下にあるファイルの`<a>`の`href`を読んで`404`かどうか確認する。

## 実行

### 前提

- `ripgrep`のインストール

### ビルド

```sh
$ go build main.go
```

### 実行結果

## その他

- ルート相対パス（`/path/to/somewhere`）は`http:localhost:8080`をつける
