package main

import (
	"log"
	"net/http"
	"strings"
)

const domain = "http://localhost:8080"

func extractURI(s string) string {
	return strings.TrimSuffix(strings.TrimPrefix(s, `href="`), "\"")
}

func isInternalLink(s string) bool {
	return strings.HasPrefix(s, "#")
}

func toAbsolutePath(domainWithSchema, rootRelativePath string) string {
	return domainWithSchema + rootRelativePath
}

func isRootRelativePath(s string) bool {
	return strings.HasPrefix(s, "/")
}

func isAbsPathWithoutProtocol(s string) bool {
	return strings.HasPrefix(s, "//")
}

func isHTTPProtocol(s string) bool {
	return strings.HasPrefix(s, "http")
}

func normarizeURI(path string) string {
	if isAbsPathWithoutProtocol(path) {
		return "https:" + path
	}

	if isRootRelativePath(path) {
		return toAbsolutePath(domain, path)
	}

	return path
}

func check404(uri string) bool {
	resp, err := http.Head(uri)
	if err != nil {
		log.Println("ERROR: on HEAD request", uri)
		return true
	}

	if resp.StatusCode == 404 {
		log.Printf("WARN: %s is 404 link.\n", uri)
		return true
	}

	log.Printf("INFO: URI=\"%s\"; StatusCode=%d", uri, resp.StatusCode)
	return false
}
