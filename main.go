package main

import (
	"bufio"
	"os"
	"os/exec"
	"strings"
)

func main() {
	wd, _ := os.Getwd()
	out, _ := exec.Command("rg", "--no-filename", "-N", "-o", "-e", `href=".*?"`, wd).Output()

	set := make(map[string]bool)
	scanner := bufio.NewScanner(strings.NewReader(string(out)))
	for scanner.Scan() {
		s := extractURI(scanner.Text())
		if isInternalLink(s) {
			continue
		}
		uri := normarizeURI(s)
		if isHTTPProtocol(uri) {
			set[uri] = true
		}
	}

	for k := range set {
		target := normarizeURI(k)
		check404(target)
	}
}
